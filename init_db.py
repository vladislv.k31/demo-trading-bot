import psycopg2 as ps
from psycopg2.extras import DictCursor, RealDictCursor
import config
import sys


connection = ps.connect(
    database = config.PG_DB_NAME,
    user = config.PG_USER,
    password = config.PG_PASSWORD,
    host = config.PG_HOST,
    port = config.PG_PORT
)
cursor = connection.cursor(cursor_factory=RealDictCursor)

if 'user' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS "user" CASCADE')
    cursor.execute('CREATE TABLE "user"(id INT GENERATED ALWAYS AS IDENTITY, chat_id INT NOT NULL UNIQUE, username VARCHAR(50), referer_id INT, balance INT DEFAULT 0 NOT NULL, bot_access BOOLEAN NOT NULL DEFAULT FALSE, signals_access BOOLEAN NOT NULL DEFAULT FALSE, zones_access BOOLEAN NOT NULL DEFAULT FALSE, PRIMARY KEY(id), CONSTRAINT fk_user FOREIGN KEY(referer_id) REFERENCES "user"(id) ON DELETE SET NULL)')

if 'signal' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS signal CASCADE')
    cursor.execute('CREATE TABLE signal(id INT GENERATED ALWAYS AS IDENTITY, json_data TEXT NOT NULL, PRIMARY KEY(id))')

if 'config' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS config CASCADE')
    cursor.execute('CREATE TABLE config(id INT GENERATED ALWAYS AS IDENTITY, user_id INT, title VARCHAR(100) NOT NULL, descr TEXT NOT NULL, tool VARCHAR(100) NOT NULL, pin SERIAL NOT NULL, is_active BOOLEAN NOT NULL DEFAULT FALSE, PRIMARY KEY(id), CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES "user"(id) ON DELETE SET NULL)')

if 'command' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS command CASCADE')
    cursor.execute('DROP TYPE IF EXISTS command_title CASCADE')
    cursor.execute("CREATE TYPE command_title AS ENUM ('A', 'B', 'C', 'D', 'E', 'F')")
    cursor.execute('CREATE TABLE command(id INT GENERATED ALWAYS AS IDENTITY, config_id INT, title command_title NOT NULL, descr TEXT NOT NULL, CONSTRAINT fk_config FOREIGN KEY(config_id) REFERENCES "config"(id) ON DELETE SET NULL)')

if 'price_list' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS price_list CASCADE')
    cursor.execute(
        'CREATE TABLE "price_list"(id INT GENERATED ALWAYS AS IDENTITY, price INT NOT NULL, days INT NOT NULL)')

if 'uploads' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS uploads CASCADE')
    cursor.execute(
        'CREATE TABLE "uploads"(id INT GENERATED ALWAYS AS IDENTITY, name VARCHAR(255) UNIQUE, path VARCHAR(255))')

if 'access' in sys.argv:
    cursor.execute('DROP TABLE IF EXISTS access CASCADE')
    cursor.execute('DROP TRIGGER IF EXISTS user_access on "user"')
    cursor.execute('DROP FUNCTION IF EXISTS set_access')

    cursor.execute(
        'CREATE TABLE "access"(id INT GENERATED ALWAYS AS IDENTITY, user_id INT, bot_access INT DEFAULT 0, signals_access INT DEFAULT 0, zones_access INT DEFAULT 0, PRIMARY KEY(id), CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES "user"(id) ON DELETE CASCADE)')

    cursor.execute('''CREATE OR REPLACE FUNCTION set_access()
      RETURNS TRIGGER 
      LANGUAGE PLPGSQL
      AS
    $$
    BEGIN
        INSERT INTO "access"(user_id) VALUES(NEW.id);
    
        RETURN NEW;
    END;
    $$''')
    cursor.execute('CREATE TRIGGER user_access AFTER INSERT ON "user" FOR EACH ROW EXECUTE PROCEDURE set_access();')

connection.commit()

cursor.close()
connection.close()
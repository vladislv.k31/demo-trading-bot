from bot import bot
import telebot
import flask
from flask import g, session
import psycopg2 as ps
from psycopg2.extras import RealDictCursor
import config
import time
import sys

from admin.routes import admin_routes

app = flask.Flask(__name__)

app.secret_key = 'secret'

# Empty webserver index, return nothing, just http 200
@app.route('/', methods=['GET', 'HEAD'])
def index():
    return ''

#db
@app.teardown_appcontext
def close_db(exception):
    db = getattr(g, '_database', None)
    cursor = getattr(g, '_cursor', None)
    if db is not None:
        db.close()
    if cursor is not None:
        cursor.close()

# admin panel
app.register_blueprint(admin_routes)

# Process webhook calls
@app.route(config.WEBHOOK_URL_PATH, methods=['POST'])
def webhook():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        flask.abort(403)
        flask.abort(403)

bot.remove_webhook()

time.sleep(1.5)

bot.set_webhook(url=config.WEBHOOK_URL_BASE + config.WEBHOOK_URL_PATH)

app.run(host=config.WEBHOOK_LISTEN, port=config.WEBHOOK_PORT, debug=True)
from bot.data import exceptions


class ConfigNotExists(Exception):

    def __init__(self):
        super().__init__(exceptions['ConfigNotExists'])


class ConfigNotAvailable(Exception):

    def __init__(self):
        super().__init__(exceptions['ConfigNotAvailable'])


class UserNotExists(Exception):

    def __init__(self):
        super().__init__(exceptions['UserNotExists'])

class PriceListEmpty(Exception):

    def __init__(self):
        super().__init__(exceptions['UserNotExists'])

class AccessNotExists(Exception):
    pass
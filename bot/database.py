import psycopg2 as ps
from psycopg2.extras import RealDictCursor
import config

from bot.exceptions import ConfigNotExists, UserNotExists, AccessNotExists, ConfigNotAvailable, PriceListEmpty

class Base:

    def __init__(self):
        self.connection = ps.connect(
            database = config.PG_DB_NAME,
            user = config.PG_USER,
            password = config.PG_PASSWORD,
            host = config.PG_HOST,
            port = config.PG_PORT
        )

        self.cursor = self.connection.cursor(cursor_factory=RealDictCursor)

    def close(self):
        self.cursor.close()
        self.connection.close()

    def query(self, query, params=[]):
        self.cursor.execute(query, params)
        self.connection.commit()

        return self.cursor.fetchall() if self.cursor.description else None

    def __del__(self):
        self.close()


class Config(Base):

    def __init__(self):
        super().__init__()

    def get_all(self):
        query = 'SELECT * FROM config'
        configs = self.query(query)

        return configs

    def get_free(self):
        query = 'SELECT * FROM config WHERE user_id is NULL'
        configs = self.query(query)

        return configs

    def get_by_user_id(self, user_id):
        query = 'SELECT * FROM config WHERE user_id = %s'
        params = [user_id]

        configs = self.query(query, params)

        return configs

    def get_by_id(self, config_id):
        query = 'SELECT * FROM config WHERE id = %s'
        params = [config_id]

        config = self.query(query, params)

        if len(config) < 1:
            raise ConfigNotExists

        return config[0]

    def get_id_by_pin(self, pin):
        query = 'SELECT id FROM config WHERE pin = %s'
        params = [pin]

        config = self.query(query, params)

        if len(config) < 1:
            raise ConfigNotExists

        return config[0]['id']

    def connect_config(self, user_id, config_id):
        configs = self.query('SELECT * FROM config WHERE user_id = null AND id = %s', (config_id,))

        if len(configs) > 0:
            raise ConfigNotAvailable

        query = 'UPDATE config SET user_id = %s WHERE id = %s'
        params = [user_id, config_id]

        self.query(query, params)

    def check_access(self, user_id, config_id):
        config = self.query('SELECT * FROM config WHERE id = %s AND user_id = %s', [config_id, user_id])
        if config:
            return True
        return False

class User(Base):

    def __init__(self):
        super().__init__()

    def check_access(self, user_id):
        access = self.query('SELECT bot_access, signals_access, zones_access FROM "user" WHERE id = %s', [user_id])

        return access[0]

    def create(self, chat_id, username = None, referer = None):
        fields = '(chat_id'
        values = '(%s'
        params = [chat_id]

        if username:
            fields += ', username'
            values += ', %s'
            params.append(username)

        if referer:
            fields += ', referer_id'
            values += ', %s'
            params.append(referer)

        fields += ')'
        values += ')'

        query = 'INSERT INTO "user"' + fields + ' VALUES' + values + ' ON CONFLICT DO NOTHING RETURNING id'

        if self.query(query, params):
            print('here >>>')
            if referer:
                referer_balance = self.get_balance(referer)
                self.set_balance(referer, referer_balance + 10)

    def set(self, user_id, field, value):
        query = 'UPDATE "user" SET {} = %s WHERE id = %s'.format(field)
        params = [value, user_id]

        self.query(query, params)

    def get_all(self):
        users = self.query('SELECT * FROM "user"')

        return users

    def get_balance(self, user_id):
        query = 'SELECT balance FROM "user" WHERE id = %s'
        params = [user_id]

        user = self.query(query, params)

        if len(user) < 1:
            raise UserNotExists

        balance = user[0]['balance']

        return balance

    def set_balance(self, user_id, balance):
        query = 'UPDATE "user" SET balance = %s WHERE id = %s'
        params = [balance, user_id]

        self.query(query, params)

    def get_id(self, chat_id):
        query = 'SELECT id FROM "user" WHERE chat_id = %s'
        params = [chat_id]

        user = self.query(query, params)

        if len(user) < 1:
            raise UserNotExists

        return user[0]['id']

    def get_by_id(self, user_id):
        query = 'SELECT * FROM "user" WHERE id = %s'
        params = [user_id]

        user = self.query(query, params)

        if len(user) < 1:
            return False

        return user[0]

class Command(Base):

    def __init__(self):
        super().__init__()

    def get(self, config_id):
        query = 'SELECT * FROM "command" WHERE config_id = %s'
        params = [config_id]

        commands = self.query(query, params)

        return commands

class Prices(Base):

    def __init__(self):
        super().__init__()

    def get_all(self):
        query = 'SELECT * FROM "price_list" ORDER by price ASC'

        prices = self.query(query)

        if len(prices) < 1:
            raise PriceListEmpty

        return prices


class Access(Base):

    def __init__(self):
        super().__init__()

    def set(self, user_id, field, value):
        query = 'UPDATE "access" SET {} = %s WHERE user_id = %s'.format(field)
        params = [value, user_id]

        self.query(query, params)

    def get(self, user_id):
        access = self.query('SELECT * FROM "access" WHERE user_id = %s', [user_id])

        if len(access) < 1:
            raise UserNotExists

        return access[0]
import telebot
from telebot import apihelper
import config
from bot.database import User


apihelper.ENABLE_MIDDLEWARE  = True

bot = telebot.TeleBot(config.API_TOKEN)
bot.remove_webhook()

pages_back = {
    'start': 'start',
    'configs': 'start',
    'account': 'start',

    'connect': 'configs',
    'setup': 'configs',

    'balance': 'account',
    'buy': 'balance',

    'admin': 'start'
}

users = {}

# middleware
@bot.middleware_handler(update_types=['message'])
def middleware(bot_instance, m):
    cid = m.chat.id
    username = m.from_user.username
    text = m.text

    referer = None
    if text != '/start' and '/start' in text:
        referer = text.replace('/start ', '')

    try:
        user = User()
        user.create(cid, username, referer)
    except Exception as e:
        print(e)
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['start'])
        return

from bot.handlers import general

from bot.handlers.admin import general
from bot.handlers.admin import set_balance
from bot.handlers.admin import send_message

from bot.handlers.account import general

from bot.handlers.balance import general

from bot.handlers.buy import general
from bot.handlers.buy import prolong_bot
from bot.handlers.buy import prolong_signals
from bot.handlers.buy import prolong_zones

from bot.handlers.configs import general
from bot.handlers.configs import connect
from bot.handlers.configs import setup
from bot.handlers.configs import start_stop

from bot.handlers import go_back
from bot.handlers import other
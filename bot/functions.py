import time

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import config


current_time = lambda: int(round(time.time() / 3600)) # current time in hours

def send_mail(to_mail, from_mail, from_password, subject, text, host='smtp.beget.com', port=25):
    print('create smtp client')
    smtp_ = smtplib.SMTP(host=host, port=port)

    print('connect account')
    smtp_.starttls()
    smtp_.login(from_mail, from_password)

    message = MIMEMultipart()

    message['From'] = from_mail
    message['To'] = to_mail
    message['Subject'] = subject
    message.attach(MIMEText(text, 'plain'))

    smtp_.send_message(message)

    del message

    smtp_.quit()

def send_command(command, type_='command'):
    send_mail(config.TO_MAIL, config.FROM_MAIL, config.FROM_PASS, type_, command)



from telebot import types


def make_keyboard(rows):
    keyboard = types.ReplyKeyboardMarkup(True, False)

    for r in rows:
        keyboard.row(*r)

    return keyboard


lang = 'en'


if lang == 'en':

    # exceptions

    exceptions = {
        'ConfigNotExists': 'Config not exists.',
        'UserNotExists': 'User not exists.',
        'ConfigNotAvailable': 'Config is not available now.',
        'PriceListEmpty': 'Price list is empty now.'
    }

    # buttons

    buttons = {
        'signals': 'Signals 💹',
        'zones': 'Day zones 🗓',
        'instructions': 'Instructions',
        'back': 'Back 🔙',

        'tr_bot': 'Trading bot 🤖',
        'my_configs': 'My configs 📑',
        'connect_bot': 'Connect ➕',
        'setup_bot': 'Setup ⚙️',
        'start_stop': 'Start/stop ⏯️',

        'ref_system': 'Ref. system 💲',
        'accesses': 'Accesses 📅',
        'my_account': 'My account 👨‍💻',
        'chat_id': 'My chat-ID',

        'balance': 'Balance 💰',
        'send_to_friend': 'Send',
        'buy': 'Spend',
        'buy_bot': 'Prolong bot',
        'buy_signals': 'Prolong signals',
        'buy_zones': 'Prolong zones',

        'save': 'Save 🆗',
        'cancel': 'Cancel ❌',

        'admin_configs': 'Admin - List Configs',
        'admin_free_configs': 'Admin - List Free Configs',
        'admin_set_balance': 'Admin - Set Balance',
        'admin_message': 'Admin - Message',
    }

    # messages


    messages = {
        'start': 'Start message!',
        'configs': 'Trading bot',
        'my-configs': 'My configs',
        'signals': 'List of signals',
        'zones': 'List of day zones',
        'account': 'My account',

        'referal_system': 'Referal system description.\nYour referal link:',
        'my_chat_id': 'Your chat-ID:',

        'balance': 'Your balance:',
        'quantity_credits': '{} credits',
        'buy': 'Choose, how you want to spend your credits',

        'accesses': {
            'accesses': 'Access to the bot will end: {}\nAccess to the signals will end: {}\nAccess to the day zones will end: {}',
            'finished': 'Finished at {}'
        },

        'connect': {
            'start': 'Config connection',
            'input_config_id': 'Choose one of available configs(Input ID - first column)',
            'input_key_id': 'Input ID key',
            'input_secret': 'Input secret key',
            'input_base_size': 'Choose base size',
            'check_data': 'Config: {}\nID key: {}\nSecret: {}\nBase size: {}',
            'show_pin': 'Your PIN-code: {} (You should remember it!)'
        },

        'setup': {
            'start': 'Config settings',
            'input_pin': 'Input PIN-code',
            'choose_cmd': 'Bot settings',
        },

        'start_stop': {
            'start': 'On/Off config',
            'input_pin': 'Input PIN-code',
            'choose_cmd': 'Bot managing',
        },

        'check_data': 'Check input data',
        'success': 'Done!',
        'cancel': 'Canceled!',
        'error': 'Oops... Something went wrong.',
        'access_denied': 'Access denied.',
        'empty': 'Empty...',

        'admin': {
            'title': 'Admin-panel',
            'set_balance': {
                'input_user_id': 'Input user id',
                'input_user_balance': 'Current user balance is {}. Input new user balance',
                'user_not_exists': 'User with this id is not exists.',
                'incorrect_value': 'Balance must be a positive number'
            },
            'send_message': {
                'input_message': 'What message do you want to send to everyone?',
                'check_data': 'Are you sure? This action cant be canceled!'
            }
        },

        'dont_understand': 'Unknown command...',

        'instructions': {
            'title': 'Instructions',
            'not_exists': 'Not accessible now...'
        },

        'send_gift': {
            'start': 'Send a gift',
            'input_chat_id': 'Input friend\'s chat-ID',
            'input_quantity': 'How many credits do you want to send?',
            'check': 'You will send {} credits to user with chat id {}. Are you sure?',
            'low_balance': 'You don\'t have enough credits for it.',
            'minimum_quantity': 'Minimum value is 1 credit.'
        },

        'prolong': {
            'input_days': 'How many days do you want?',
            'check_data': '{} will be prolonged by {} day(s). Are you sure?',
            'min_value': 'Minimum value is 1 day',
            'low_balance': 'You don\'t have enough credits for it.'
        },

        'check_access': {
            'bot_access': 'Access for using trading bot ended. All your configs stopped. To continue using it you need to prolong it.'
        }
    }
    # keyboards

    keyboards = {
        'start': make_keyboard([[buttons['tr_bot']], [buttons['signals'], buttons['zones']], [buttons['my_account'], buttons['instructions']]]),
        'configs': make_keyboard([[buttons['connect_bot'], buttons['setup_bot'], buttons['start_stop']],
                                  [buttons['my_configs'], buttons['back']]]),
        'account': make_keyboard([[buttons['ref_system'], buttons['chat_id']], [buttons['balance'], buttons['accesses'], buttons['back']]]),

        'balance': make_keyboard([[buttons['buy'], buttons['send_to_friend']], [buttons['back']]]),
        'buy': make_keyboard([[buttons['buy_bot']], [buttons['buy_signals'], buttons['buy_zones']], [buttons['back']]]),

        'connect': {
            'base_size': make_keyboard([['10', '150', '250'], [buttons['cancel']]])
        },

        'setup': {
            'cmd': make_keyboard([['COMM A', 'COMM B', 'COMM C'], ['COMM D', 'COMM E', 'COMM F'], [buttons['cancel']]])
        },

        'start_stop': {
            'cmd': make_keyboard([['RUNBULL', 'RUNBEAR', 'STOP'], [buttons['cancel']]])
        },

        'check_data': make_keyboard([[buttons['save'], buttons['cancel']]]),
        'cancel': make_keyboard([[buttons['cancel']]]),
        'hide': types.ReplyKeyboardRemove(),

        'admin': make_keyboard([[buttons['admin_configs'], buttons['admin_free_configs']], [buttons['admin_set_balance'], buttons['admin_message']], [buttons['back']]]),

        'price_list': make_keyboard([['1', '14', '30']])
    }
else:

    # exceptions

    exceptions = {
        'ConfigNotExists': 'Конфига с таким ID не существует.',
        'UserNotExists': 'Пользователя с таким Чат-ID не существует.'
    }

    # buttons

    buttons = {
        'signals': 'Signals',
        'zones': 'Day zones',
        'back': 'Back',

        'tr_bot': 'Trading bot',
        'my_configs': 'My configs',
        'connect_bot': 'Connect ➕',
        'setup_bot': 'Setup ⚙️',
        'start_stop': 'Start/stop ⏯️',

        'ref_system': 'Ref. system',
        'my_account': 'My account',
        'chat_id': 'My chat-ID',

        'balance': 'Balance',
        'send_to_friend': 'Переслать другу',
        'buy': 'Потратить',
        'buy_bot': 'Продлить бота',
        'buy_signals': 'Продлить сигналы',
        'buy_zones': 'Продлить зоны',

        'save': 'Save',
        'cancel': 'Cancel'
    }

    # messages

    messages = {
        'start': 'Приветственное сообщение!',
        'configs': 'Торговый бот',
        'my-configs': 'Мои конфиги:',
        'signals': 'Список сигналов',
        'zones': 'Список дневных зон',
        'account': 'Мой аккаунт',

        'referal_system': 'Описание реферальной системы.\nВаша реферальная ссылка:',
        'my_chat_id': 'Ваш чат-ID:',

        'balance': 'Ваш баланс:',
        'quantity_credits': '{} кредитов',
        'buy': 'Выберите, куда вы хотите потратить свои кредиты',

        'connect': {
            'start': 'Подключение конфига',
            'input_config_id': 'Выберите один из доступных конфигов(Укажите ID - первый столбец)',
            'input_key_id': 'Введите ID ключа',
            'input_secret': 'Введите Secret ключа',
            'input_base_size': 'Выберите базовый размер контрактов',
            'check_data': 'Config: {}\nID key: {}\nSecret: {}\nBase size: {}'
        },
        'setup': {
            'start': 'Настройка конфига',
            'input_pin': 'Укажите PIN-код конфига',
            'choose_cmd': 'Управление ботом',
        },

        'start_stop': {
            'start': 'Включение/Выключение конфига',
            'input_pin': 'Укажите PIN-код конфига',
            'choose_cmd': 'Управление ботом',
        },

        'check_data': 'Перепроверьте данные',
        'success': 'Сделано!',
        'cancel': 'Отменено',
        'error': 'Упс... Что-то пошло не так.',
        'access_denied': 'Нет доступа.',
        'empty': 'Пусто...',

        'admin': 'Админ-панель',

        'dont_understand': 'Неизвестная команда...',

        'send_gift': {
            'start': 'Переслать кредиты другу',
            'input_chat_id': 'Укажите Чат-ID друга',
            'input_quantity': 'Укажите кол-во кредитов'
        }
    }

    # keyboards

    keyboards = {
        'start': make_keyboard([[buttons['tr_bot']], [buttons['signals'], buttons['zones']], [buttons['my_account']]]),
        'configs': make_keyboard([[buttons['connect_bot'], buttons['setup_bot'], buttons['start_stop']],
                                  [buttons['my_configs'], buttons['back']]]),
        'account': make_keyboard([[buttons['ref_system'], buttons['chat_id']], [buttons['balance'], buttons['back']]]),

        'balance': make_keyboard([[buttons['buy'], buttons['send_to_friend']], [buttons['back']]]),
        'buy': make_keyboard([[buttons['buy_bot']], [buttons['buy_signals'], buttons['buy_zones']], [buttons['back']]]),

        'connect': {
            'base_size': make_keyboard([['50', '100', '150'], [buttons['cancel']]])
        },

        'setup': {
            'cmd': make_keyboard([['COMM A', 'COMM B', 'COMM C'], ['COMM D', 'COMM E', 'COMM F'], [buttons['cancel']]])
        },

        'start_stop': {
            'cmd': make_keyboard([['RUNBULL', 'RUNBEAR', 'STOP'], [buttons['cancel']]])
        },

        'check_data': make_keyboard([[buttons['save'], buttons['cancel']]]),
        'cancel': make_keyboard([[buttons['cancel']]]),
        'hide': types.ReplyKeyboardRemove(),

        'admin': make_keyboard([['Конфиги', 'Команды', 'Доступы'], ['Юзеры', 'Сообщение'], ['Назад']])
    }


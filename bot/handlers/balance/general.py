from bot import bot
from bot import data
from bot import users
from bot.database import User
from bot.helpers import set_page
from bot.helpers import check_cancel
from bot.exceptions import UserNotExists


# my balance handler

@bot.message_handler(func=lambda m: m.text == data.buttons['balance'])
@set_page('balance')
def balance(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
        user_balance = user.get_balance(user_id)

        bot.send_message(cid, data.messages['balance'], reply_markup=data.keyboards['balance'])
        bot.send_message(cid, data.messages['quantity_credits'].format(user_balance))
    except Exception as e:
        print(e)
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['account'])

# send to fried

@bot.message_handler(func=lambda m: m.text == data.buttons['send_to_friend'])
@set_page('balance')
def send_to_friend(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['send_gift']['start'], reply_markup=data.keyboards['cancel'])

    msg = bot.send_message(cid, data.messages['send_gift']['input_chat_id'])
    bot.register_next_step_handler(msg, input_chat_id)

@check_cancel
def input_chat_id(m):
    cid = m.chat.id
    text = m.text

    try:
        friend_chat_id = int(text)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['balance'])
        return

    try:
        user = User()
        user_id = user.get_id(friend_chat_id)
    except UserNotExists as e:
        bot.send_message(cid, e, reply_markup=data.keyboards['balance'])
        return
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['balance'])
        return

    input_data = {
        'friend_id': friend_chat_id
    }

    msg = bot.send_message(cid, data.messages['send_gift']['input_quantity'], reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_quantity, input_data)

@check_cancel
def input_quantity(m, input_data):
    cid = m.chat.id
    text = m.text

    try:
        quantity = int(text)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['balance'])
        return

    if quantity < 1:
        bot.send_message(cid, data.messages['send_gift']['minimum_quantity'], reply_markup=data.keyboards['balance'])
        return

    input_data['quantity'] = quantity

    msg = bot.send_message(cid, data.messages['send_gift']['check'].format(input_data['quantity'], input_data['friend_id']), reply_markup=data.keyboards['check_data'])
    bot.register_next_step_handler(msg, check_data, input_data)

def check_data(m, input_data):
    cid = m.chat.id
    text = m.text

    if text == data.buttons['save']:
        try:
            user = User()

            user_id = user.get_id(cid)
            friend_id = user.get_id(input_data['friend_id'])

            user_balance = user.get_balance(user_id)
            friend_balance = user.get_balance(friend_id)

            if user_balance < input_data['quantity']:
                bot.send_message(cid, data.messages['send_gift']['low_balance'], reply_markup=data.keyboards['balance'])
                return

            user.set_balance(user_id, user_balance - input_data['quantity'])
            user.set_balance(friend_id, friend_balance + input_data['quantity'])
        except:
            bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['balance'])

        bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['balance'])
    elif text == data.buttons['cancel']:
        bot.send_message(cid, data.messages['cancel'], reply_markup=data.keyboards['balance'])
    else:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['balance'])

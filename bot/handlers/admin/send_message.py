from bot import bot
from bot import data
from bot import users
from config import ADMIN_IDS
from bot.database import Config, User
from bot.exceptions import UserNotExists
from bot.helpers import check_cancel

# decorator for check access

def check_id(func):
    def wrapper(m):
        cid = m.chat.id
        if cid not in ADMIN_IDS:
            from bot.handlers.other import dont_understand
            dont_understand(m)
            return
        func(m)
    return wrapper

# decorator for set page

def set_page(func):
    def wrapper(m, *args, **kwargs):
        cid = m.chat.id
        users[cid] = 'admin'
        func(m, *args, **kwargs)

    return wrapper


# admin handler

@bot.message_handler(func=lambda m: m.text == data.buttons['admin_message'])
@set_page
@check_id
def admin_send_message(m):
    cid = m.chat.id

    msg = bot.send_message(cid, data.messages['admin']['send_message']['input_message'], reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_message)

@check_cancel
def input_message(m):
    cid = m.chat.id
    message = m.text

    input_data = {
        'message': message
    }

    msg = bot.send_message(cid, data.messages['admin']['send_message']['check_data'], reply_markup=data.keyboards['check_data'])
    bot.register_next_step_handler(msg, check_data, input_data)

def check_data(m, input_data):
    cid = m.chat.id
    text = m.text

    if text == data.buttons['save']:
        try:
            user = User()
            all_users = user.get_all()

            for u in all_users:
                bot.send_message(u['chat_id'], input_data['message'])

            bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['admin'])
        except:
            bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
            return
    else:
        bot.send_message(cid, data.messages['cancel'], reply_markup=data.keyboards['admin'])
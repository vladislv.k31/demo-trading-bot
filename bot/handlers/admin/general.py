from bot import bot
from bot import data
from bot import users
from config import ADMIN_IDS
from bot.database import Config

# decorator for check access

def check_id(func):
    def wrapper(m):
        cid = m.chat.id
        if cid not in ADMIN_IDS:
            from bot.handlers.other import dont_understand
            dont_understand(m)
            return
        func(m)
    return wrapper

# decorator for set page

def set_page(func):
    def wrapper(m, *args, **kwargs):
        cid = m.chat.id
        users[cid] = 'admin'
        func(m, *args, **kwargs)

    return wrapper


# admin handler

@bot.message_handler(commands=['admin'])
@set_page
@check_id
def admin(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['admin']['title'], reply_markup=data.keyboards['admin'])

# list configs

@bot.message_handler(func=lambda m: m.text == data.buttons['admin_configs'])
@set_page
@check_id
def admin_configs(m):
    cid = m.chat.id

    try:
        config = Config()
        configs = config.get_all()
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
        return

    if len(configs) < 1:
        bot.send_message(cid, data.messages['empty'])
    else:
        reply = ''

        for c in configs:
            reply += f'{c["id"]} - {c["title"]}  -  {c["descr"]}  -  {c["tool"]}\n\n'

        bot.send_message(cid, reply)

# list free configs

@bot.message_handler(func=lambda m: m.text == data.buttons['admin_free_configs'])
@set_page
@check_id
def admin_free_configs(m):
    cid = m.chat.id

    try:
        config = Config()
        configs = config.get_free()
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
        return

    if len(configs) < 1:
        bot.send_message(cid, data.messages['empty'])
    else:
        reply = ''

        for c in configs:
            reply += f'{c["id"]} - {c["title"]}  -  {c["descr"]}  -  {c["tool"]}\n\n'

        bot.send_message(cid, reply)
from bot import bot
from bot import data
from bot import users
from config import ADMIN_IDS
from bot.database import Config, User
from bot.exceptions import UserNotExists
from bot.helpers import check_cancel

# decorator for check access

def check_id(func):
    def wrapper(m):
        cid = m.chat.id
        if cid not in ADMIN_IDS:
            from bot.handlers.other import dont_understand
            dont_understand(m)
            return
        func(m)
    return wrapper

# decorator for set page

def set_page(func):
    def wrapper(m, *args, **kwargs):
        cid = m.chat.id
        users[cid] = 'admin'
        func(m, *args, **kwargs)

    return wrapper


# admin handler

@bot.message_handler(func=lambda m: m.text == data.buttons['admin_set_balance'])
@set_page
@check_id
def admin_set_balance(m):
    cid = m.chat.id

    try:
        user = User()
        all_users = user.get_all()
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
        return

    if len(all_users) < 1:
        bot.send_message(cid, data.messages['empty'])
    else:
        reply = ''

        for u in all_users:
            reply += f'{u["id"]} - {u["chat_id"]}  -  {u["username"]}  -  {u["balance"]}\n\n'

        bot.send_message(cid, reply)
        msg = bot.send_message(cid, data.messages['admin']['set_balance']['input_user_id'], reply_markup=data.keyboards['cancel'])
        bot.register_next_step_handler(msg, input_user_id)

@check_cancel
def input_user_id(m):
    cid = m.chat.id
    text = m.text

    try:
        user_id = int(text)
        user = User()

        balance = user.get_balance(user_id)
    except UserNotExists:
        bot.send_message(cid, data.messages['admin']['set_balance']['user_not_exists'], reply_markup=data.keyboards['admin'])
        return
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
        return

    input_data = {
        'user_id': user_id
    }

    msg = bot.send_message(cid, data.messages['admin']['set_balance']['input_user_balance'].format(balance), reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_user_balance, input_data)

@check_cancel
def input_user_balance(m, input_data):
    cid = m.chat.id
    text = m.text

    try:
        new_balance = int(text)

        if new_balance < 0:
            bot.send_message(cid, data.messages['admin']['set_balance']['incorrect_value'], reply_markup=data.keyboards['admin'])
            return

        user = User()
        user.set_balance(input_data['user_id'], new_balance)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['admin'])
        return

    bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['admin'])
from bot import bot
from bot import data
from bot import users, pages_back


@bot.message_handler(func=lambda message: True)
def dont_understand(m):
    cid = m.chat.id

    users[cid] = 'start'

    bot.send_message(cid, data.messages['dont_understand'], reply_markup=data.keyboards['start'])


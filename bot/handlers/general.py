from bot import bot
from bot import data
from bot import pages_back
from bot.database import User
from bot.helpers import set_page
from bot import users, pages_back
import os


# start messsage

@bot.message_handler(commands=['start'])
@set_page('start')
def start(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['start'], reply_markup=data.keyboards['start'])

# signals handler

@bot.message_handler(func=lambda m: m.text == data.buttons['signals'])
@set_page('start')
def signals(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
        access = user.check_access(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['start'])
        return

    bot.send_message(cid, data.messages['signals'])

    if not access['signals_access']:
        bot.send_message(cid, data.messages['access_denied'], reply_markup=data.keyboards['start'])
        return

    bot.send_message(cid, data.messages['empty'], reply_markup=data.keyboards['start'])

# zones handler

@bot.message_handler(func=lambda m: m.text == data.buttons['zones'])
@set_page('start')
def zones(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
        access = user.check_access(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['start'])
        return

    bot.send_message(cid, data.messages['zones'])

    if not access['zones_access']:
        bot.send_message(cid, data.messages['access_denied'], reply_markup=data.keyboards['start'])
        return

    bot.send_message(cid, data.messages['empty'], reply_markup=data.keyboards['start'])

# instructions handler

@bot.message_handler(func=lambda m: m.text == data.buttons['instructions'])
@set_page('start')
def instructions(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['instructions']['title'], reply_markup=data.keyboards['start'])

    if os.path.exists('./uploads/instructions.pdf'):
        f = open('./uploads/instructions.pdf', 'rb')
        bot.send_document(cid, f)
    else:
        bot.send_message(cid, data.messages['instructions']['empty'])
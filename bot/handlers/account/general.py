from bot import bot
from bot import data
from bot import users
from bot.database import User, Access
from bot.helpers import set_page
from bot.functions import current_time

import datetime


# my account handler

@bot.message_handler(func=lambda m: m.text == data.buttons['my_account'])
@set_page('account')
def account(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['account'], reply_markup=data.keyboards['account'])

# referal system handler

@bot.message_handler(func=lambda m: m.text == data.buttons['ref_system'])
@set_page('account')
def referal_system(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['account'])
        return

    bot.send_message(cid, data.messages['referal_system'])
    bot.send_message(cid, 'https://telegram.me/forleodevbot?start=' + str(user_id), reply_markup=data.keyboards['account'])

# my chat id handler

@bot.message_handler(func=lambda m: m.text == data.buttons['chat_id'])
@set_page('account')
def my_chat_id(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['my_chat_id'])
    bot.send_message(cid, str(cid), reply_markup=data.keyboards['account'])

# user's accesses

@bot.message_handler(func=lambda m: m.text == data.buttons['accesses'])
@set_page('account')
def accesses(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)

        access = Access()
        accesses = access.get(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['account'])
        return

    bot_access = datetime.datetime.fromtimestamp(accesses['bot_access'] * 3600.0)
    signals_access = datetime.datetime.fromtimestamp(accesses['signals_access'] * 3600.0)
    zones_access = datetime.datetime.fromtimestamp(accesses['zones_access'] * 3600.0)

    if current_time() > accesses['bot_access']:
        bot_access = data.messages['accesses']['finished'].format(bot_access)

    if current_time() > accesses['signals_access']:
        signals_access = data.messages['accesses']['finished'].format(signals_access)

    if current_time() > accesses['zones_access']:
        zones_access = data.messages['accesses']['finished'].format(zones_access)

    bot.send_message(cid, data.messages['accesses']['accesses'].format(bot_access, signals_access, zones_access))

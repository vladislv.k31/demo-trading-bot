from bot import bot
from bot import data
from bot import users
from bot.database import Config, User, Command
from bot.exceptions import ConfigNotExists
from bot.helpers import check_cancel, set_page


# setup handler

@bot.message_handler(func=lambda m: m.text == data.buttons['setup_bot'])
@set_page('configs')
def setup(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
        access = user.check_access(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    bot.send_message(cid, data.messages['setup']['start'])

    if not access['bot_access']:
        bot.send_message(cid, data.messages['access_denied'], reply_markup=data.keyboards['configs'])
        return

    msg = bot.send_message(cid, data.messages['setup']['input_pin'], reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_config_pin)

@check_cancel
def input_config_pin(m):
    cid = m.chat.id
    text = m.text

    try:
        pin = int(text)

        user = User()
        user_id = user.get_id(cid)

        config = Config()
        config_id = config.get_id_by_pin(pin)

        if not config.check_access(user_id, config_id):
            bot.send_message(cid, data.messages['access_denied'], reply_markup=data.keyboards['configs'])
            return
    except ConfigNotExists as e:
        bot.send_message(cid, e, reply_markup=data.keyboards['configs'])
        return
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    command = Command()
    commands = command.get(config_id)

    commands_info = {
        'A': 'Change the number of contracts to 10',
        'B': 'Change the number of contracts to 150',
        'C': 'Change the number of contracts to 250',
        'D': 'Not assigned',
        'E': 'Not assigned',
        'F': 'Not assigned',
    }

    for c in commands:
        commands_info[c['title']] = c['descr']

    reply = ''

    for c in commands_info.keys():
        reply += 'COMM ' + c + ' - ' + commands_info[c] + '\n'

    msg = bot.send_message(cid, data.messages['setup']['choose_cmd'], reply_markup=data.keyboards['setup']['cmd'])
    bot.send_message(cid, reply)
    bot.register_next_step_handler(msg, choose_cmd, pin)

@check_cancel
def choose_cmd(m, pin):
    cid = m.chat.id
    cmd = m.text

    if cmd in ['COMM A', 'COMM B', 'COMM C', 'COMM D', 'COMM E', 'COMM F']:
        print(cmd)
        bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['configs'])
    else:
        bot.send_message(cid, data.messages['error'],reply_markup=data.keyboards['configs'])

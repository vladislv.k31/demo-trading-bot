from bot import bot
from bot import data
from bot import users
from bot.database import Config, User
from bot.exceptions import ConfigNotExists, ConfigNotAvailable
from bot.helpers import check_cancel, set_page
from bot import settings
from bot.functions import send_command


# handlers for connect bot

@bot.message_handler(func=lambda m: m.text == data.buttons['connect_bot'])
@set_page('configs')
def connect_bot(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)
        access = user.check_access(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    bot.send_message(cid, data.messages['connect']['start'])

    if not access['bot_access']:
        bot.send_message(cid, data.messages['access_denied'], reply_markup=data.keyboards['configs'])
        return

    try:
        config = Config()
        free_configs = config.get_free()
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    bot.send_message(cid, data.messages['connect']['input_config_id'])

    if len(free_configs) < 1:
        bot.send_message(cid, data.messages['empty'], reply_markup=data.keyboards['configs'])
        return

    reply = ''

    for c in free_configs:
        reply += f'{c["id"]} - {c["title"]} - {c["tool"]} - {c["descr"]}\n'

    print('finish')

    msg = bot.send_message(cid, reply, reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_config_id)

@check_cancel
def input_config_id(m):
    cid = m.chat.id
    text = m.text

    try:
        config_id = int(text)
        config = Config()
        choosed_config = config.get_by_id(config_id)
    except ConfigNotExists as e:
        bot.send_message(cid, e, reply_markup=data.keyboards['configs'])
        return
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    input_data = {
        'config': choosed_config
    }

    msg = bot.send_message(cid, data.messages['connect']['input_key_id'], reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_key_id, input_data)

@check_cancel
def input_key_id(m, input_data):
    cid = m.chat.id
    text = m.text

    try:
        key_id = int(text)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    input_data['key_id'] = key_id

    msg = bot.send_message(cid, data.messages['connect']['input_secret'], reply_markup=data.keyboards['cancel'])
    bot.register_next_step_handler(msg, input_secret, input_data)

@check_cancel
def input_secret(m, input_data):
    cid = m.chat.id
    text = m.text

    input_data['secret_id'] = text

    msg = bot.send_message(cid, data.messages['connect']['input_base_size'], reply_markup=data.keyboards['connect']['base_size'])
    bot.register_next_step_handler(msg, input_base_size, input_data)

@check_cancel
def input_base_size(m, input_data):
    cid = m.chat.id
    text = m.text

    try:
        base_size = int(text)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    if base_size not in [10, 150, 250]:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    input_data['base_size'] = base_size

    bot.send_message(cid, data.messages['check_data'])

    reply = data.messages['connect']['check_data'].format(input_data["config"]["title"], input_data["key_id"], input_data["secret_id"], input_data["base_size"])

    msg = bot.send_message(cid, reply, reply_markup=data.keyboards['check_data'])
    bot.register_next_step_handler(msg, check_connect, input_data)

def check_connect(m, input_data):
    cid = m.chat.id
    action = m.text

    if action == data.buttons['save']:
        try:
            user = User()
            user_id = user.get_id(cid)

            # command = settings.add_command
            # send_command(command)

            config = Config()
            config.connect_config(user_id, input_data['config']['id'])
        except ConfigNotAvailable as e:
            bot.send_message(cid, e, reply_markup=data.keyboards['configs'])
            return
        except Exception as e:
            print(e)
            bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
            return

        bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['configs'])
        bot.send_message(cid, data.messages['connect']['show_pin'].format(input_data['config']['pin']))

    elif action == data.buttons['cancel']:
        bot.send_message(cid, data.messages['cancel'], reply_markup=data.keyboards['configs'])
    else:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])

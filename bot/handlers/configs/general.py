from bot import bot
from bot import data
from bot import users
from bot.database import Config, User
from bot.exceptions import ConfigNotExists
from bot.helpers import set_page


# configs handler

@bot.message_handler(func=lambda m: m.text == data.buttons['tr_bot'])
@set_page('configs')
def configs(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['configs'], reply_markup=data.keyboards['configs'])

# my configs list handler

@bot.message_handler(func=lambda m: m.text == data.buttons['my_configs'])
@set_page('configs')
def my_configs(m):
    cid = m.chat.id

    try:
        user = User()
        user_id = user.get_id(cid)

        config = Config()
        configs = config.get_by_user_id(user_id)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['configs'])
        return

    bot.send_message(cid, data.messages['my-configs'], reply_markup=data.keyboards['configs'])

    if len(configs) < 1:
        bot.send_message(cid, data.messages['empty'])
    else:
        reply = ''

        for c in configs:
            reply += f'{c["title"]}  -  {c["descr"]}  -  {c["tool"]}\n\n'

        bot.send_message(cid, reply)
from bot import bot
from bot import data
from bot import users
from bot.database import User
from bot.helpers import set_page


# spend credits handler

@bot.message_handler(func=lambda m: m.text == data.buttons['buy'])
@set_page('buy')
def buy(m):
    cid = m.chat.id

    bot.send_message(cid, data.messages['buy'], reply_markup=data.keyboards['buy'])
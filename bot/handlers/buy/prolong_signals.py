from bot import bot
from bot import data
from bot import users
from bot.database import User, Access, Prices
from bot.helpers import set_page, check_cancel
from bot.functions import current_time
from bot.exceptions import PriceListEmpty
from bot.data import make_keyboard


# spend credits handler

@bot.message_handler(func=lambda m: m.text == data.buttons['buy_signals'])
@set_page('buy')
def buy(m):
    cid = m.chat.id

    try:
        prices = Prices()
        price_list = prices.get_all()
    except PriceListEmpty as e:
        bot.send_message(cid, e, reply_markup=data.keyboards['buy'])
        return
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
        return

    bot.send_message(cid, 'Choose one of price list')

    try:
        reply = ''
        keyboard = []

        print(price_list)

        idx = 0
        for p in price_list:
            idx += 1
            reply += '{} - {} days = {} credits\n'.format(idx, p['days'], p['price'])
            keyboard.append(str(idx))
    except Exception as e:
        print(e)
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
        return

    try:
        msg = bot.send_message(cid, reply, reply_markup=make_keyboard([keyboard, [data.buttons['cancel']]]))
        bot.register_next_step_handler(msg, choose_price, keyboard, price_list)
    except Exception as e:
        print(e)
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
        return

@check_cancel
def choose_price(m, keyboard, price_list):
    cid = m.chat.id
    text = m.text

    try:
        item = int(text)
    except:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
        return

    if str(item) not in keyboard:
        bot.send_message(cid, 'Incorrect price list choose.', reply_markup=data.keyboards['buy'])
        return

    msg = bot.send_message(cid, 'You will prolong signals for {} days by {} credits'.format(price_list[item - 1]['days'], price_list[item - 1]['price']), reply_markup=data.keyboards['check_data'])
    bot.register_next_step_handler(msg, check_data, item, price_list)

def check_data(m, item, price_list):
    cid = m.chat.id
    text = m.text

    price = price_list[item - 1]['price']
    days = price_list[item - 1]['days']

    if text == data.buttons['save']:
        try:
            user = User()
            user_id = user.get_id(cid)
            balance = user.get_balance(user_id)
        except:
            bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
            return

        if price > balance:
            bot.send_message(cid, data.messages['prolong']['low_balance'], reply_markup=data.keyboards['buy'])
            return

        try:
            access = Access()
            user_access = access.get(user_id)

            if user_access['signals_access'] < current_time():
                new_access = current_time() + (days * 24)
            else:
                new_access = user_access['signals_access'] + (days * 24)

            access.set(user_id, 'signals_access', new_access)
            user.set(user_id, 'signals_access', True)
            user.set_balance(user_id, balance - price)

        except:
            bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
            return

        bot.send_message(cid, data.messages['success'], reply_markup=data.keyboards['buy'])

    elif text == data.buttons['cancel']:
        bot.send_message(cid, data.messages['cancel'], reply_markup=data.keyboards['buy'])
    else:
        bot.send_message(cid, data.messages['error'], reply_markup=data.keyboards['buy'])
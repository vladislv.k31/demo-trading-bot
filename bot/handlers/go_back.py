from bot import bot
from bot import data
from bot import pages_back
from bot import users, pages_back

from bot.handlers.general import start
from bot.handlers.configs.general import configs
from bot.handlers.account.general import account
from bot.handlers.balance.general import balance
from bot.handlers.buy.general import buy
from bot.handlers.admin.general import admin


pages_handlers = {
    'start': start,
    'configs': configs,
    'account': account,
    'balance': balance,
    'buy': buy,
    'admin': admin
}


# back command

@bot.message_handler(func=lambda m: m.text == data.buttons['back'])
def go_back(m):
    cid = m.chat.id

    if cid in list(users.keys()):
        page = users[cid]
    else:
        page = 'start'

    next_page = pages_back[page]
    users[cid] = next_page

    try:
        pages_handlers[next_page](m)
    except:
        bot.send_message(cid, data.messages['start'], reply_markup=data.keyboards['start'])
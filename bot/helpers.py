from bot import users
from bot import bot
from bot import data


def set_page(page):
    def inner_decorator(func):
        def wrapped(m, *args, **kwargs):
            cid = m.chat.id
            users[cid] = page
            func(m, *args, **kwargs)
        return wrapped
    return inner_decorator

def check_cancel(func):
    def wrapper(m, *args, **kwargs):
        cid = m.chat.id
        text = m.text
        if text == data.buttons['cancel']:
            bot.send_message(cid, data.messages['cancel'], reply_markup=data.keyboards[users[cid]])
            return
        func(m, *args, **kwargs)
    return wrapper
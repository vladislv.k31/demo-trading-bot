import psycopg2 as ps
from psycopg2.extras import DictCursor, RealDictCursor
import config
import sys


connection = ps.connect(
    database = config.PG_DB_NAME,
    user = config.PG_USER,
    password = config.PG_PASSWORD,
    host = config.PG_HOST,
    port = config.PG_PORT
)
cursor = connection.cursor(cursor_factory=RealDictCursor)

if 'user' in sys.argv:
    cursor.execute('INSERT INTO "user"(chat_id, username) VALUES(%s, %s)', (463758574, 'vlad_kandyba'))

if 'price_list' in sys.argv:
    cursor.execute('INSERT INTO "price_list"(price, days) VALUES(%s, %s)', (1, 1))
    cursor.execute('INSERT INTO "price_list"(price, days) VALUES(%s, %s)', (12, 14))
    cursor.execute('INSERT INTO "price_list"(price, days) VALUES(%s, %s)', (24, 30))

if 'config' in sys.argv:
    cursor.execute('INSERT INTO config(title, descr, tool, pin) VALUES(%s, %s, %s, %s)', ('First Config', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Config Tool', 12345))
    cursor.execute('INSERT INTO config(title, descr, tool, pin) VALUES(%s, %s, %s, %s)', (
    'Second Config', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Config Tool', 55555))

connection.commit()

cursor.close()
connection.close()
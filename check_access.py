from bot.functions import current_time
from bot.database import User, Access
from bot import bot
from bot import data


user = User()
users = user.get_all()

access = Access()

fields = ['bot_access', 'signals_access', 'zones_access']

for u in users:
    user_access = access.get(u['id'])

    for f in fields:
        if user_access[f] < current_time() and u[f] == True:
            user.set(u['id'], f, False)
            if f == 'bot_access':
                bot.send_message(u['chat_id'], data.messages['check_access'][f])
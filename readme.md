## Demo Trading TeleBot

How to use:
- Clone repo
- Make python environment: ```python3 -m venv venv```
- Start using venv: ```. venv/bin/activate```
- Install packages: ```pip install -r requirements.txt```
- Make config file ```config.py```:

```
    API_TOKEN = 'TOKEN'
    
    HOST = 'http://localhost:8443'

    # webhook settings
    WEBHOOK_PORT = 8443
    WEBHOOK_LISTEN = '127.0.0.1'
    WEBHOOK_URL_BASE = 'https://e7265b4e6a40.ngrok.io'
    WEBHOOK_URL_PATH = '/%s/' % API_TOKEN
    
    # database settings
    PG_DB_NAME = ''
    PG_USER = ''
    PG_PASSWORD = ''
    PG_HOST = '127.0.0.1'
    PG_PORT = 5432
    
    # admin-panel settings
    ADMIN_LOGIN = ''
    ADMIN_PASSWORD = ''
    
    ADMIN_IDS = []
    
    # smtp settings
    TO_MAIL = ''
    FROM_MAIL = ''
    FROM_PASS = ''
```

- Run bot: ```python main.py```
from flask import Blueprint, render_template, g, session, request, redirect, url_for, send_from_directory
import psycopg2 as ps
from psycopg2.extras import RealDictCursor
import config
from bot import bot
from bot.database import Access, User
from bot.functions import current_time
from werkzeug.utils import secure_filename
import os
import config

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

ALLOWED_EXTENSIONS = {'pdf'}
UPLOAD_FOLDER = './uploads'

# db
def get_db():
    db = getattr(g, '_database', None)
    cursor = getattr(g, '_cursor', None)

    if db is None:
        db = g._database = ps.connect(
            database=config.PG_DB_NAME,
            user=config.PG_USER,
            password=config.PG_PASSWORD,
            host=config.PG_HOST,
            port=config.PG_PORT
        )
        cursor = g._cursor = db.cursor(cursor_factory=RealDictCursor)
    else:
        cursor = g._cursor = db.cursor(cursor_factory=RealDictCursor)

    return db, cursor


admin_routes = Blueprint('admin-panel', __name__, url_prefix='/admin-panel', template_folder='templates', static_folder='static')

# index page
@admin_routes.route('/')
def index():
    return redirect(url_for('admin-panel.login'))

# login page
@admin_routes.route('/login', methods=['POST', 'GET'])
def login():
    if "user" in session:
        return redirect(url_for("admin-panel.configs"))

    if request.method == 'POST':
        user_login = request.form["login"]
        password = request.form["password"]
        if password == config.ADMIN_PASSWORD and user_login == config.ADMIN_LOGIN:
            session["user"] = 1
            return 'ok'
        return 'error'

    return render_template('login.html')

# logout
@admin_routes.route('/logout')
def logout():
    session.pop("user", None)
    return redirect(url_for("admin-panel.login"))

# dashboard page
@admin_routes.route('/dashboard')
def dashboard():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    return render_template('dashboard.html')

# configs page
@admin_routes.route('/dashboard/configs', methods=['POST', 'GET', 'DELETE', 'PUT'])
def configs():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'DELETE':
        try:
            config_id = request.form['config_id']

            db, cur = get_db()

            cur.execute('DELETE FROM config WHERE id = %s', (config_id,))
            db.commit()

            return 'ok'
        except:
            return 'Что-то пошло не так...'

    if request.method == 'PUT':
        try:
            id = request.form['id']
            title = request.form["title"]
            descr = request.form["descr"]
            tool = request.form["tool"]

            db, cur = get_db()

            cur.execute('UPDATE "config" SET title = %s, descr = %s, tool = %s WHERE id = %s', (title, descr, tool, id))
            db.commit()

            return 'ok'
        except:
            return 'Что-то пошло не так...'

    if request.method == 'POST':
        try:
            title = request.form["title"]
            descr = request.form["descr"]
            tool = request.form["tool"]
            pin = request.form["pin"]

            db, cur = get_db()

            cur.execute('SELECT * FROM "config" WHERE pin = %s', (pin,))

            cfg = cur.fetchall()
            if len(cfg) > 0:
                return 'PIN уже занят'

            cur.execute('INSERT INTO "config"(title, descr, tool, pin) VALUES(%s, %s, %s, %s)',
                        (title, descr, tool, pin))
            db.commit()

            return 'ok'
        except:
            return 'Что-то пошло не так...'

    try:
        active_filter = 'all'
        if 'filter' in request.args:
            if request.args['filter'] in ['all', 'free', 'used']:
                active_filter = request.args['filter']

        db, cur = get_db()

        if active_filter == 'free':
            cur.execute('SELECT * FROM "config" WHERE user_id IS null ORDER by id DESC')
        elif active_filter == 'used':
            cur.execute('SELECT * FROM "config" WHERE user_id IS NOT null ORDER by id DESC')
        else:
            cur.execute('SELECT * FROM "config" ORDER by id DESC')

        cfgs = cur.fetchall()
    except:
        return 'Что-то пошло не так'

    return render_template('configs.html', configs=cfgs, active_filter=active_filter)

# update config
@admin_routes.route('/dashboard/configs/update/<id>')
def update_config(id):
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    try:
        db, cur = get_db()
        cur.execute('SELECT * FROM "config" WHERE id = %s', (id,))
        config = cur.fetchall()

        if len(config) < 1:
            return 'Конфига с таким айди не существует!'
    except:
        return 'Что-то пошло не так...'

    return render_template('config_update.html', config=config[0])

# commands page
@admin_routes.route('/dashboard/commands', methods=['GET', 'POST', 'DELETE'])
def commands():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'DELETE':
        try:
            command_id = request.form['command_id']

            db, cur = get_db()

            cur.execute('DELETE FROM command WHERE id = %s', (command_id,))
            db.commit()

            return 'ok'
        except:
            return 'Что-то пошло не так...'

    if request.method == 'POST':
        try:
            config_id = request.form["config_id"]
            title = request.form["title"]
            descr = request.form["descr"]

            db, cur = get_db()

            cur.execute('SELECT * FROM "config" WHERE id = %s', (config_id,))
            cfgs = cur.fetchall()

            if len(cfgs) < 1:
                return 'Конфига с таким ID не существует'

            cur.execute('SELECT * FROM "command" WHERE title = %s AND config_id = %s', (title, config_id))

            cmnd = cur.fetchall()
            if len(cmnd) > 0:
                return 'Команде уже назначена'

            cur.execute('INSERT INTO "command"(config_id, title, descr) VALUES(%s, %s, %s)',
                        (config_id, title, descr))
            db.commit()

            return 'ok'
        except Exception as e:
            print(e)
            return 'Что-то пошло не так...'

    try:
        db, cur = get_db()

        cur.execute('SELECT * FROM "command" ORDER by id DESC')
        cmnds = cur.fetchall()

        cur.execute('SELECT id, title FROM "config" ORDER by id DESC')
        cfgs_ids = cur.fetchall()
    except:
        return 'Что-то пошло не так'

    return render_template('commands.html', commands=cmnds, cfgs_ids=cfgs_ids)

# users page
@admin_routes.route('/dashboard/users')
def users():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    try:
        db, cur = get_db()

        cur.execute('SELECT * FROM "user" ORDER by id DESC')
        usrs = cur.fetchall()
    except:
        return 'Что-то пошло не так'

    return render_template('users.html', usrs=usrs)

# update user
@admin_routes.route('/dashboard/users/update/<id>')
def update_user(id):
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    try:
        db, cur = get_db()
        cur.execute('SELECT * FROM "user" WHERE id = %s', (id,))
        user = cur.fetchall()

        if len(user) < 1:
            return 'Юзера с таким айди не существует!'
    except:
        return 'Что-то пошло не так...'

    return render_template('user_update.html', user=user[0])


# update user balance
@admin_routes.route('/dashboard/users/balance', methods=['PUT'])
def update_user_balance():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'PUT':
        try:
            id = request.form['id']
            balance = request.form["balance"]

            db, cur = get_db()

            cur.execute('UPDATE "user" SET balance = %s WHERE id = %s', (balance, id))
            db.commit()

            return 'ok'
        except:
            return 'Что-то пошло не так...'


# update user access
@admin_routes.route('/dashboard/users/access', methods=['GET', 'PUT'])
def update_user_access():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'PUT':
        try:
            id = request.form['id']
            bot_access = True if request.form["bot_access"] == 'on' else False
            signals_access = True if request.form["signals_access"] == 'on' else False
            zones_access = True if request.form["zones_access"] == 'on' else False

            db, cur = get_db()

            cur.execute('UPDATE "user" SET bot_access = %s, signals_access = %s, zones_access = %s WHERE id = %s', (bot_access, signals_access, zones_access, id))
            db.commit()

            return 'ok'
        except Exception as e:
            print(e)
            return 'Что-то пошло не так...'

    return 'ok'

# update user prolong
@admin_routes.route('/dashboard/users/prolong', methods=['GET', 'PUT'])
def prolong_user_access():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'PUT':
        try:
            id = request.form['id']
            what = request.form['prolong']
            days = int(request.form['days'])

            access = Access()
            user = User()
            user_access = access.get(id)

            if user_access[what] < current_time():
                new_access = current_time() + (days * 24)
            else:
                new_access = user_access[what] + (days * 24)

            access.set(id, what, new_access)
            user.set(id, what, True)

            return 'ok'
        except Exception as e:
            print(e)
            return 'Что-то пошло не так...'

    return 'ok'

# instructions
@admin_routes.route('/dashboard/instructions', methods=['GET', 'POST'])
def instructions():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'POST':
        try:
            # check if the post request has the file part
            if 'file' not in request.files:
                return 'No file part'
            file = request.files['file']
            # if user does not select file, browser also
            # submit an empty part without filename
            if file.filename == '':
                return 'No file selected'
            if file and allowed_file(file.filename):
                print(dir(file))
                file.save(os.path.join(UPLOAD_FOLDER, 'instructions.pdf'))

                db, cur = get_db()

                cur.execute('INSERT INTO uploads(name, path) VALUES(%s, %s) ON CONFLICT DO NOTHING', ('instructions.pdf', config.HOST + '/admin-panel/uploads/instructions.pdf'))
                db.commit()

                return 'ok'
            else:
                return 'Not this format'
        except:
            return 'Что-то пошло не так...'

    db, cur = get_db()

    cur.execute('SELECT * FROM uploads WHERE name = %s', ('instructions.pdf',))
    uploads = cur.fetchall()

    return render_template('instructions.html', upload=None if len(uploads) < 1 else uploads[0])

@admin_routes.route('/uploads/<filename>')
def uploaded_file(filename):
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    return send_from_directory(UPLOAD_FOLDER,
                               filename)

# message page
@admin_routes.route('/dashboard/messages', methods=['GET', 'POST'])
def messages():
    if "user" not in session:
        return redirect(url_for("admin-panel.login"))

    if request.method == 'POST':
        try:
            message = request.form["message"]

            db, cur = get_db()

            cur.execute('SELECT chat_id FROM "user"')
            all_users = cur.fetchall()

            for u in all_users:
                bot.send_message(u['chat_id'], message)

            return 'ok'
        except Exception as e:
            print(e)
            return 'Что-то пошло не так...'

    return render_template('messages.html')